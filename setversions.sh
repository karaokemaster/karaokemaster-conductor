export SHORT_VERSION=$(gitversion /output json /showvariable MajorMinorPatch)
export BUNDLE_VERSION=$(gitversion /output json /showvariable PreReleaseNumber)
if [[ -z "$BUNDLE_VERSION" ]]; then
    export BUNDLE_VERSION=0
fi

echo "Short version $SHORT_VERSION"
echo "Bundle version $BUNDLE_VERSION"

plutil -replace CFBundleShortVersionString -string $SHORT_VERSION ./KaraokeMaster.Conductor/Info.plist
plutil -replace CFBundleVersion -string $BUNDLE_VERSION ./KaraokeMaster.Conductor/Info.plist
