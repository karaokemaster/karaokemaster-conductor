/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Collections.Generic;
using System.Linq;
using AppKit;
using KaraokeMaster.Conductor.Models;
using KaraokeMaster.Conductor.Preferences;

namespace KaraokeMaster.Conductor
{
	public partial class LightingCuesViewController : NSViewController
	{
        public LightingCuesViewController (IntPtr handle) : base (handle)
		{
		}

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            var dataSource = new LightingCuesDataSource();

            // Base entries; uneditable
            dataSource.Cues.Add(new Cue { Index = 0, Name = "Idle" });
            dataSource.Cues.Add(new Cue { Index = 1, Name = "Starting" });
            dataSource.Cues.Add(new Cue { Index = 2, Name = "Playing" });

            Enumerable
                .Range(3, 124) // Second arg is count, not max
                .Select(i => AppDelegate.Settings.Cues.FirstOrDefault(q => q.Index == i) ?? new Cue { Index = (byte)i })
                .ToList()
                .ForEach(cue => dataSource.Cues.Add(cue));

            dataSource.Cues.Add(new Cue { Index = 127, Name = "Blackout" });

            CueList.DataSource = dataSource;
            CueList.Delegate = new LightingCuesTableDelegate(dataSource);
        }

        public override void ViewWillDisappear()
        {
            base.ViewWillDisappear();

            var cues = new List<Cue>();

            AppDelegate.Settings.Cues = (CueList.DataSource as LightingCuesDataSource)
                .Cues
                .OrderBy(c => c.Index)
                .Where(c => c.Index > 2 && !string.IsNullOrWhiteSpace(c.Name) && (c.Theme || !string.IsNullOrWhiteSpace(c.Color)))
                .ToList();

            AppDelegate.Settings.Save();
        }
    }
}
