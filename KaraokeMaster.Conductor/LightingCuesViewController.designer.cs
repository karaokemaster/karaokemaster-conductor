// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace KaraokeMaster.Conductor
{
	[Register ("LightingCuesViewController")]
	partial class LightingCuesViewController
	{
		[Outlet]
		AppKit.NSTableColumn CueColorColumn { get; set; }

		[Outlet]
		AppKit.NSTableColumn CueIndexColumn { get; set; }

		[Outlet]
		AppKit.NSTableView CueList { get; set; }

		[Outlet]
		AppKit.NSTableColumn CueNameColumn { get; set; }

		[Outlet]
		AppKit.NSTableColumn CueNoteColumn { get; set; }

		[Outlet]
		AppKit.NSTableColumn CueStateColumn { get; set; }

		[Outlet]
		AppKit.NSTableColumn CueTestColumn { get; set; }

		[Outlet]
		AppKit.NSTableColumn CueThemeColumn { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CueColorColumn != null) {
				CueColorColumn.Dispose ();
				CueColorColumn = null;
			}

			if (CueIndexColumn != null) {
				CueIndexColumn.Dispose ();
				CueIndexColumn = null;
			}

			if (CueList != null) {
				CueList.Dispose ();
				CueList = null;
			}

			if (CueNameColumn != null) {
				CueNameColumn.Dispose ();
				CueNameColumn = null;
			}

			if (CueNoteColumn != null) {
				CueNoteColumn.Dispose ();
				CueNoteColumn = null;
			}

			if (CueStateColumn != null) {
				CueStateColumn.Dispose ();
				CueStateColumn = null;
			}

			if (CueTestColumn != null) {
				CueTestColumn.Dispose ();
				CueTestColumn = null;
			}

			if (CueThemeColumn != null) {
				CueThemeColumn.Dispose ();
				CueThemeColumn = null;
			}
		}
	}
}
