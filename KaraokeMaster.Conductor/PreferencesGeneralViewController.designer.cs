// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace KaraokeMaster.Conductor
{
	[Register ("PreferencesGeneralViewController")]
	partial class PreferencesGeneralViewController
	{
		[Outlet]
		AppKit.NSPopUpButton MidiInterfaceList { get; set; }

		[Outlet]
		AppKit.NSTextField PlaybackStartWebhook { get; set; }

		[Outlet]
		AppKit.NSPopUpButton PlaybackStartWebhookMethod { get; set; }

		[Outlet]
		AppKit.NSTextField PlaybackStopWebhook { get; set; }

		[Outlet]
		AppKit.NSPopUpButton PlaybackStopWebhookMethod { get; set; }

		[Outlet]
		AppKit.NSPopUpButton ServerList { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (MidiInterfaceList != null) {
				MidiInterfaceList.Dispose ();
				MidiInterfaceList = null;
			}

			if (PlaybackStartWebhook != null) {
				PlaybackStartWebhook.Dispose ();
				PlaybackStartWebhook = null;
			}

			if (PlaybackStopWebhook != null) {
				PlaybackStopWebhook.Dispose ();
				PlaybackStopWebhook = null;
			}

			if (ServerList != null) {
				ServerList.Dispose ();
				ServerList = null;
			}

			if (PlaybackStartWebhookMethod != null) {
				PlaybackStartWebhookMethod.Dispose ();
				PlaybackStartWebhookMethod = null;
			}

			if (PlaybackStopWebhookMethod != null) {
				PlaybackStopWebhookMethod.Dispose ();
				PlaybackStopWebhookMethod = null;
			}
		}
	}
}
