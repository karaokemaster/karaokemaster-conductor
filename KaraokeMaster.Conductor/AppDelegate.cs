﻿/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using AppKit;
using Foundation;
using KaraokeMaster.Conductor.Services;

namespace KaraokeMaster.Conductor
{
    [Register("AppDelegate")]
    public class AppDelegate : NSApplicationDelegate
    {
        public static Settings Settings;
        public static ApiDiscoveryService ApiDiscoveryService;

        public AppDelegate()
        {
            Settings = new Settings();
            Settings.Load();

            ApiDiscoveryService = new ApiDiscoveryService();
        }

        public override void DidFinishLaunching(NSNotification notification)
        {
            // Insert code here to initialize your application
        }

        public override void WillTerminate(NSNotification notification)
        {
            // Insert code here to tear down your application
            ApiDiscoveryService.Dispose();
        }
    }
}
