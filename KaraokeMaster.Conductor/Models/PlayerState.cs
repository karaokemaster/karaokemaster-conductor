﻿using System;
namespace KaraokeMaster.Conductor.Models
{
    public class PlayerState
    {
        public PlaybackState State { get; set; }
        public int? QueueId { get; set; }
        public decimal? VideoTime { get; set; }
        public decimal? VideoDuration { get; set; }
    }

    public enum PlaybackState
    {
        Stopped = 0,
        Playing = 1,
        Starting = 2,
        StoppingAfter = 3,
        Failed = -2,
        Disconnected = -1,
    }
}
