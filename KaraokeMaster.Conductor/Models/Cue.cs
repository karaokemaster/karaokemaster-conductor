﻿/*
    
    Copyright 2020-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

namespace KaraokeMaster.Conductor.Models
{
    public class Cue
    {
        public Cue()
        {
            State = CueState.Unknown;
        }

        public Cue (byte index, string name, bool theme, string color)
            : this()
        {
            Index = index;
            Name = name;
            Theme = theme;
            Color = color;
        }

        public byte Index { get; set; }
        public string Name { get; set; }
        public bool Theme { get; set; }
        public string Color { get; set; }

        public CueState State { get; set; }
    }

    public enum CueState
    {
        Unknown = -1,
        Off = 0,
        On = 1,
    }
}

