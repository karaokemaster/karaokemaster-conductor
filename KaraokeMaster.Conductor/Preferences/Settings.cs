﻿using System;
using Foundation;
using KaraokeMaster.Conductor.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KaraokeMaster.Conductor
{
    public class Settings
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Settings _settings;

        private const string FILE_NAME = "Settings";

        public string MidiDevice { get; set; }
        public string BaseUrl { get; set; }
        public string PlaybackStartWebhook { get; set; }
        public string PlaybackStartWebhookMethod { get; set; }
        public string PlaybackStopWebhook { get; set; }
        public string PlaybackStopWebhookMethod { get; set; }
        public List<Cue> Cues { get; set; }

        public event EventHandler OnUpdate;
        public event TestCueEventHandler OnTestCue;

        public delegate void TestCueEventHandler(Cue cue);

        public void Load()
        {
            //var path = NSBundle.MainBundle.PathForResource(FILE_NAME, "plist");
            //var rootDict = NSDictionary.FromFile(path);

            var defaults = NSUserDefaults.StandardUserDefaults;

            var midiDevice = defaults.StringForKey("MidiDevice");
            var baseUrl = defaults.StringForKey("BaseUrl");
            var playbackStartWebhook = defaults.StringForKey("PlaybackStartWebhook");
            var playbackStartWebhookMethod = defaults.StringForKey("PlaybackStartWebhookMethod");
            var playbackStopWebhook = defaults.StringForKey("PlaybackStopWebhook");
            var playbackStopWebhookMethod = defaults.StringForKey("PlaybackStopWebhookMethod"); 

            var cues = defaults.ValueForKey((NSString)"Cues") as NSArray;
            // var cues = defaults.ArrayForKey("Cues");

            if (!string.IsNullOrEmpty(midiDevice))
                MidiDevice = midiDevice;

            if (!string.IsNullOrEmpty(baseUrl))
                BaseUrl = baseUrl;

            if (!string.IsNullOrEmpty(playbackStartWebhook))
                PlaybackStartWebhook = playbackStartWebhook;
            if (!string.IsNullOrEmpty(playbackStartWebhookMethod))
                PlaybackStartWebhookMethod = playbackStartWebhookMethod;

            if (!string.IsNullOrEmpty(playbackStopWebhook))
                PlaybackStopWebhook = playbackStopWebhook;
            if (!string.IsNullOrEmpty(playbackStopWebhookMethod))
                PlaybackStopWebhookMethod = playbackStopWebhookMethod;

            Cues = new List<Cue>();

            if (cues != null)
            {
                for (nuint i = 0; i < cues.Count; i++)
                {
                    var cue = cues.GetItem<NSDictionary>(i);
                    var q = new Cue
                    {
                        Index = (byte)(cue["Index"] as NSNumber),
                        Name = (cue["Name"] as NSString),
                        Theme = (bool)(cue["Theme"] as NSNumber),
                        Color = (cue["Color"] as NSString)
                    };

                    Cues.Add(q);
                }
            }
        }

        public void Save()
        {
            //var rootDict = new NSMutableDictionary<NSString, NSObject>();
            var defaults = NSUserDefaults.StandardUserDefaults;

            defaults.SetString(MidiDevice ?? "", "MidiDevice");
            defaults.SetString(BaseUrl ?? "", "BaseUrl");
            defaults.SetString(PlaybackStartWebhook ?? "", "PlaybackStartWebhook");
            defaults.SetString(PlaybackStartWebhookMethod ?? "", "PlaybackStartWebhookMethod");
            defaults.SetString(PlaybackStopWebhook ?? "", "PlaybackStopWebhook");
            defaults.SetString(PlaybackStopWebhookMethod ?? "", "PlaybackStopWebhookMethod");

            var cues = NSArray.FromNSObjects(
                Cues.Select(cue =>
                {
                    var q = new NSMutableDictionary<NSString, NSObject>();
                    q["Index"] = (NSNumber)cue.Index;
                    q["Name"] = (NSString)cue.Name;
                    q["Theme"] = (NSNumber)cue.Theme;
                    q["Color"] = (NSString)(cue.Color ?? "");
                    return (NSObject)q;
                }).ToArray());

            defaults.SetValueForKey(cues, (NSString)"Cues");

            NSUserDefaults.StandardUserDefaults.Synchronize();

            //var path = NSBundle.MainBundle.PathForResource(FILE_NAME, "plist");
            //rootDict.WriteToFile(path, true);

            OnUpdate?.Invoke(this, null);
        }

        public void TestCue(Cue cue) => OnTestCue?.Invoke(cue);
    }
}
