﻿using System;
using System.Collections.Generic;
using AppKit;
using KaraokeMaster.Conductor.Models;

namespace KaraokeMaster.Conductor.Preferences
{
    public class LightingCuesDataSource : NSTableViewDataSource
    {
        public LightingCuesDataSource()
        {
        }

        public List<Cue> Cues { get; } = new List<Cue>();

        public override nint GetRowCount(NSTableView tableView) => Cues.Count;
    }
}
