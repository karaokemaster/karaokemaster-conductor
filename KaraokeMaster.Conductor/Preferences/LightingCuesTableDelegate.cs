﻿using System;
using AppKit;
using CoreGraphics;
using Foundation;

namespace KaraokeMaster.Conductor.Preferences
{
    public class LightingCuesTableDelegate : NSTableViewDelegate
    {
        private LightingCuesDataSource DataSource;

        public LightingCuesTableDelegate(LightingCuesDataSource dataSource)
        {
            DataSource = dataSource;
        }

        public override NSView GetViewForItem(NSTableView tableView, NSTableColumn tableColumn, nint row)
        {
            // TODO: Refactor into less mess...

            var cue = DataSource.Cues[(int)row];

            // This pattern allows you reuse existing views when they are no-longer in use.
            // If the returned view is null, you instance up a new view
            // If a non-null view is returned, you modify it enough to reflect the new data
            var view = tableView.MakeView(tableColumn.Identifier + "Cell", this);
            if (view == null)
            {
                view = new NSTableCellView();


                switch (tableColumn.Identifier)
                {
                    case "Index":
                        var indexView = new NSTextField
                        {
                            Identifier = tableColumn.Identifier + "Cell",
                            BackgroundColor = NSColor.Clear,
                            Bordered = false,
                            Selectable = false,
                            Editable = false,
                            Alignment = NSTextAlignment.Right,
                            Tag = row
                        };
                        view = indexView;
                        break;
                    case "Note":
                        var noteView = new NSTextField
                        {
                            Identifier = tableColumn.Identifier + "Cell",
                            BackgroundColor = NSColor.Clear,
                            Bordered = false,
                            Selectable = false,
                            Editable = false,
                            Alignment = NSTextAlignment.Right,
                            Tag = row
                        };
                        view = noteView;
                        break;
                    case "Name":
                        var nameView = new NSTextField
                        {
                            Identifier = tableColumn.Identifier + "Cell",
                            BackgroundColor = NSColor.Clear,
                            Bordered = false,
                            Selectable = false,
                            Editable = cue.Index > 2,
                            Tag = row
                        };
                        nameView.EditingEnded += NameView_EditingEnded;
                        view = nameView;
                        break;
                    case "Theme":
                        var themeView = new NSTableCellView
                        {
                            Identifier = tableColumn.Identifier + "Cell"
                        };
                        var themeButton = new NSButton(new CGRect(0, 0, 16, 16))
                        {
                            Identifier = "ThemeButton",
                            Bordered = false,
                            Enabled = cue.Index > 2,
                            Tag = row
                        };
                        themeButton.SetButtonType(NSButtonType.Switch);
                        themeButton.Activated += ThemeButton_Activated;
                        themeView.AddSubview(themeButton);
                        view = themeView;
                        break;
                    case "Color":
                        var colorView = new NSTableCellView
                        {
                            Identifier = tableColumn.Identifier + "Cell"
                        };
                        var colorWell = new NSColorWell(new CGRect(0, 0, tableColumn.Width, 16))
                        {
                            Identifier = "ColorWell",
                            Tag = row,
                            // Bordered = false,
                            Enabled = cue.Index > 2,
                        };
                        colorWell.Activated += ColorWell_Activated;
                        colorView.AddSubview(colorWell);
                        view = colorView;
                        break;
                    case "Test":
                        var testView = new NSTableCellView
                        {
                            Identifier = tableColumn.Identifier + "Cell"
                        };
                        var testButton = new NSButton(new CGRect(0, 0, 16, 16))
                        {
                            Identifier = "TestButton",
                            // TODO: Switch to icon
                            Title = "Test",
                            Tag = row
                        };
                        testButton.Activated += TestButton_Activated;
                        testView.AddSubview(testButton);
                        view = testView;
                        break;
                    case "State":
                        var stateView = new NSTextField
                        {
                            Identifier = tableColumn.Identifier + "Cell",
                            BackgroundColor = NSColor.Clear,
                            Bordered = false,
                            Selectable = false,
                            Editable = false,
                            Tag = row
                        };
                        view = stateView;
                        break;
                }
            }

            // Setup view based on the column selected
            view.Identifier = tableColumn.Identifier + "Cell";

            switch (tableColumn.Identifier)
            {
                case "Index":
                    (view as NSTextField).StringValue = cue.Index.ToString();
                    break;
                case "Note":
                    var octave = (cue.Index / 12) - 2;
                    string note = "";

                    switch (cue.Index % 12)
                    {
                        case 0:
                            note = $"C{octave}";
                            break;
                        case 1:
                            note = $"C#{octave}";
                            break;
                        case 2:
                            note = $"D{octave}";
                            break;
                        case 3:
                            note = $"D#{octave}";
                            break;
                        case 4:
                            note = $"E{octave}";
                            break;
                        case 5:
                            note = $"F{octave}";
                            break;
                        case 6:
                            note = $"F#{octave}";
                            break;
                        case 7:
                            note = $"G{octave}";
                            break;
                        case 8:
                            note = $"G#{octave}";
                            break;
                        case 9:
                            note = $"A{octave}";
                            break;
                        case 10:
                            note = $"A#{octave}";
                            break;
                        case 11:
                            note = $"B{octave}";
                            break;
                    }
                    (view as NSTextField).StringValue = note;
                    break;
                case "Name":
                    (view as NSTextField).StringValue = cue.Name ?? "";
                    break;
                case "Theme":
                    var themeButton = (NSButton)(view as NSTableCellView).Subviews[0];
                    themeButton.State = cue.Theme ? NSCellStateValue.On : NSCellStateValue.Off;
                    break;
                case "Color":
                    var colorWell = (NSColorWell)(view as NSTableCellView).Subviews[0];
                    // colorWell.Color = NSColor.FromRgba(0, 0, 0, 0);
                    break;
                case "State":
                    (view as NSTextField).StringValue = cue.State.ToString();
                    break;
            }

            return view;
        }

        private void NameView_EditingEnded(object sender, EventArgs e)
        {
            var notification = (NSNotification)sender;
            var view = (NSTextField)notification.Object;

            DataSource.Cues[(int)view.Tag].Name = view.StringValue;
        }

        private void ThemeButton_Activated(object sender, EventArgs e)
        {
            var view = (NSButton)sender;

            DataSource.Cues[(int)view.Tag].Theme = view.State == NSCellStateValue.On;
        }

        private void ColorWell_Activated(object sender, EventArgs e)
        {
            var view = (NSColorWell)sender;

            // TODO: Fix and convert to a proper hex string
            DataSource.Cues[(int)view.Tag].Color = view.Color.ToString();
        }

        private void TestButton_Activated(object sender, EventArgs e)
        {
            var view = (NSButton)sender;

            var cue = DataSource.Cues[(int)view.Tag];
            AppDelegate.Settings.TestCue(cue);
        }
    }
}
