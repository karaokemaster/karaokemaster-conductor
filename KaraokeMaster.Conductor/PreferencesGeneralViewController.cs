/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Linq;
using AppKit;

namespace KaraokeMaster.Conductor
{
	public partial class PreferencesGeneralViewController : NSViewController
	{
        private bool _isLoaded = false;

		public PreferencesGeneralViewController (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidAppear()
        {
            base.ViewDidAppear();

            AppDelegate.ApiDiscoveryService.OnUpdate += PopulateServerList;

            // TODO: Refresh lists while dialog is open
            PopulateServerList();
            PopulateMidiDeviceList();

            PlaybackStartWebhook.StringValue = AppDelegate.Settings.PlaybackStartWebhook ?? "";
            PlaybackStartWebhookMethod.SelectItem(AppDelegate.Settings.PlaybackStartWebhookMethod ?? "GET");
            PlaybackStopWebhook.StringValue = AppDelegate.Settings.PlaybackStopWebhook ?? "";
            PlaybackStopWebhookMethod.SelectItem(AppDelegate.Settings.PlaybackStopWebhookMethod ?? "GET");

            _isLoaded = true;
        }

        public override void ViewWillDisappear()
        {
            base.ViewWillDisappear();

            if (!_isLoaded) return;

            AppDelegate.ApiDiscoveryService.OnUpdate -= null;

            if (ServerList.IndexOfSelectedItem > 0)
                AppDelegate.Settings.BaseUrl = ServerList.TitleOfSelectedItem;
            else
                AppDelegate.Settings.BaseUrl = "";

            if (MidiInterfaceList.IndexOfSelectedItem > 1)
                AppDelegate.Settings.MidiDevice = MidiInterfaceList.TitleOfSelectedItem;
            else
                AppDelegate.Settings.MidiDevice = "";

            if (!string.IsNullOrWhiteSpace(PlaybackStartWebhook.StringValue))
            {
                AppDelegate.Settings.PlaybackStartWebhook = PlaybackStartWebhook.StringValue;
                AppDelegate.Settings.PlaybackStartWebhookMethod = PlaybackStartWebhookMethod.TitleOfSelectedItem;
            }
            else
            {
                AppDelegate.Settings.PlaybackStartWebhook = null;
                AppDelegate.Settings.PlaybackStartWebhookMethod = null;
            }

            if (!string.IsNullOrWhiteSpace(PlaybackStopWebhook.StringValue))
            {
                AppDelegate.Settings.PlaybackStopWebhook = PlaybackStopWebhook.StringValue;
                AppDelegate.Settings.PlaybackStopWebhookMethod = PlaybackStopWebhookMethod.TitleOfSelectedItem;
            }
            else
            {
                AppDelegate.Settings.PlaybackStopWebhook = null;
                AppDelegate.Settings.PlaybackStopWebhookMethod = null;
            }

            AppDelegate.Settings.Save();
            _isLoaded = false;
        }

        private void PopulateServerList()
        {
            InvokeOnMainThread(() =>
            {
                while (ServerList.ItemCount > 3) ServerList.RemoveItem(1);

                AppDelegate
                    .ApiDiscoveryService
                    .Instances
                    .ForEach(i => ServerList.InsertItem(i, 1));

                ServerList.ItemAtIndex(0).Enabled = false;

                if (!string.IsNullOrEmpty(AppDelegate.Settings.BaseUrl) && ServerList.ItemWithTitle(AppDelegate.Settings.BaseUrl) != null)
                {
                    ServerList.SelectItem(AppDelegate.Settings.BaseUrl);
                }
            });
        }

        private void PopulateMidiDeviceList()
        {
            InvokeOnMainThread(() =>
            {
                while (MidiInterfaceList.ItemCount > 2) MidiInterfaceList.RemoveItem(2);

                var devices = MidiInterface.Devices;

                if (devices.Count < 1 && string.IsNullOrEmpty(AppDelegate.Settings.MidiDevice))
                {
                    // Hide the separator
                    MidiInterfaceList.ItemAtIndex(1).Hidden = true;
                    return;
                }

                // Show the separator
                MidiInterfaceList.ItemAtIndex(1).Hidden = false;

                devices
                    .ToList()
                    .ForEach(kvp =>
                    {
                        var id = kvp.Key;
                        var name = kvp.Value;

                        MidiInterfaceList.InsertItem(name, 2);
                        MidiInterfaceList.ItemAtIndex(2).Identifier = id;
                    });

                if (!string.IsNullOrEmpty(AppDelegate.Settings.MidiDevice))
                {
                    // Ensure the device exists so we can select it, and hope it comes back
                    if (MidiInterfaceList.ItemWithTitle(AppDelegate.Settings.MidiDevice) == null)
                    {
                        MidiInterfaceList.InsertItem(AppDelegate.Settings.MidiDevice, 2);
                        MidiInterfaceList.ItemAtIndex(2).Enabled = false;
                    }

                    MidiInterfaceList.SelectItem(AppDelegate.Settings.MidiDevice);
                }
            });
        }
    }
}
