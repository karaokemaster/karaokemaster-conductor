// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace KaraokeMaster.Conductor
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSTextField BeatDetectorLatency { get; set; }

		[Outlet]
		AppKit.NSLevelIndicator BeatDetectorStatus { get; set; }

		[Outlet]
		AppKit.NSLevelIndicator BeatStatus { get; set; }

		[Outlet]
		AppKit.NSTextField Bpm { get; set; }

		[Outlet]
		AppKit.NSLevelIndicator ConnectionStatus { get; set; }

		[Outlet]
		AppKit.NSTextField DebugWarning { get; set; }

		[Outlet]
		AppKit.NSTextField HostName { get; set; }

		[Outlet]
		AppKit.NSTextField MidiDevice { get; set; }

		[Outlet]
		AppKit.NSLevelIndicator MidiStatus { get; set; }

		[Outlet]
		AppKit.NSTextField PlayerState { get; set; }

		[Outlet]
		AppKit.NSLevelIndicator PlayerStatus { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (BeatDetectorLatency != null) {
				BeatDetectorLatency.Dispose ();
				BeatDetectorLatency = null;
			}

			if (BeatDetectorStatus != null) {
				BeatDetectorStatus.Dispose ();
				BeatDetectorStatus = null;
			}

			if (BeatStatus != null) {
				BeatStatus.Dispose ();
				BeatStatus = null;
			}

			if (Bpm != null) {
				Bpm.Dispose ();
				Bpm = null;
			}

			if (ConnectionStatus != null) {
				ConnectionStatus.Dispose ();
				ConnectionStatus = null;
			}

			if (HostName != null) {
				HostName.Dispose ();
				HostName = null;
			}

			if (MidiDevice != null) {
				MidiDevice.Dispose ();
				MidiDevice = null;
			}

			if (MidiStatus != null) {
				MidiStatus.Dispose ();
				MidiStatus = null;
			}

			if (PlayerState != null) {
				PlayerState.Dispose ();
				PlayerState = null;
			}

			if (PlayerStatus != null) {
				PlayerStatus.Dispose ();
				PlayerStatus = null;
			}

			if (DebugWarning != null) {
				DebugWarning.Dispose ();
				DebugWarning = null;
			}
		}
	}
}
