﻿/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Threading.Tasks;
using AppKit;
using Foundation;
using KaraokeMaster.Conductor.Models;

namespace KaraokeMaster.Conductor
{
    public partial class ViewController : NSViewController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly MidiInterface _midiInterface = new MidiInterface();
        private readonly PlayerStateMonitor _playerStateMonitor = new PlayerStateMonitor();
        private readonly LatencyMonitor _latencyMonitor = new LatencyMonitor();
        private readonly LightingCueMonitor _lightingCueMonitor;
        private readonly WebhookMonitor _webhookMonitor;
        private readonly BeatScheduler _beatScheduler;
        private readonly SystemSleepManager _systemSleepManager;
        private ConnectionManager _connectionManager;
        private double _tempo;

        public ViewController(IntPtr handle) : base(handle)
        {
            _beatScheduler = new BeatScheduler(_midiInterface.SendBeatTick, 24);

            _lightingCueMonitor = new LightingCueMonitor(_midiInterface, _playerStateMonitor);
            _webhookMonitor = new WebhookMonitor(_playerStateMonitor);
            _connectionManager = new ConnectionManager(_beatScheduler, _playerStateMonitor, _latencyMonitor, _lightingCueMonitor, _webhookMonitor);

            _systemSleepManager = new SystemSleepManager();

            _connectionManager.OnConnect += ServerConnected;
            _connectionManager.OnDisconnect += ServerDisconnected;
            _connectionManager.OnBeatReceived += BeatReceived;

            _midiInterface.OnConnect += MidiConnected;
            _midiInterface.OnDisconnect += MidiDisconnect;

            _playerStateMonitor.OnUpdate += PlayerStateUpdate;

            _latencyMonitor.OnUpdate += LatencyUpdate;

            AppDelegate.Settings.OnUpdate += SettingsChanged;
        }

        public override async void ViewDidLoad()
        {
            base.ViewDidLoad();

#if DEBUG
            Log.Info("Starting in Debug mode...");
            DebugWarning.Hidden = false;
#else
            Log.Info("Starting in Release mode...");
#endif

            ResetStatus();
        }

        public override async void ViewDidAppear()
        {
            base.ViewDidAppear();

            _midiInterface.Connect(AppDelegate.Settings.MidiDevice);

            if (string.IsNullOrEmpty(AppDelegate.Settings.BaseUrl))
                PerformSegue("OpenPreferences", this);
            else
                await StartConnection();

            _systemSleepManager.DisableSleep();
        }

        public override async void ViewWillDisappear()
        {
            base.ViewWillDisappear();

            _systemSleepManager.EnableSleep();

            await _connectionManager.Stop();
        }

        public override void ViewDidDisappear()
        {
            base.ViewDidDisappear();

            NSApplication.SharedApplication.Terminate(NSApplication.SharedApplication);
        }

        private async Task StartConnection()
        {
            if (_connectionManager.Connected)
                await _connectionManager.Stop();

            var serviceUrl = AppDelegate.Settings.BaseUrl;
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
                serviceUrl = "http://localhost:5000/";
#endif

            InvokeOnMainThread(() =>
            {
                HostName.StringValue = serviceUrl;
            });

            await _connectionManager.Start(serviceUrl);

        }

        private async void ServerConnected(object sender, EventArgs e)
        {
            InvokeOnMainThread(() =>
            {
                ConnectionStatus.Cell.DoubleValue = 1.0;
                HostName.StringValue = _connectionManager.BaseUrl;
            });
        }

        private async void ServerDisconnected(object sender, EventArgs e)
        {
            InvokeOnMainThread(() =>
            {
                ConnectionStatus.Cell.DoubleValue = 0.0;
                HostName.StringValue = _connectionManager.BaseUrl;
            });
        }

        private async void BeatReceived(double tempo)
        {
            _tempo = tempo;

            Task.Run(async () =>
            {
                InvokeOnMainThread(() =>
                {
                    Bpm.StringValue = $"{tempo:0} bpm";
                    BeatStatus.Cell.DoubleValue = 1.0;
                });

                await Task.Delay(35);
                InvokeOnMainThread(() =>
                {
                    BeatStatus.Cell.DoubleValue = 0.0;
                });
            });
        }

        private async void MidiConnected(object sender, EventArgs e)
        {
            var displayName = _midiInterface.Name;
            if (string.IsNullOrEmpty(displayName)) displayName = "KaraokeMaster output";

            InvokeOnMainThread(() =>
            {
                MidiStatus.Cell.DoubleValue = 1.0;
                MidiDevice.StringValue = displayName;
            });
        }

        private async void MidiDisconnect(object sender, EventArgs e)
        {
            var displayName = _midiInterface.Name;
            if (string.IsNullOrEmpty(displayName)) displayName = "KaraokeMaster output";

            InvokeOnMainThread(() =>
            {
                MidiStatus.Cell.DoubleValue = 0.0;
                MidiDevice.StringValue = displayName;
            });
        }

        private async void PlayerStateUpdate(PlaybackState state)
        {
            // TODO: Fix this
            // if (!online || !playing) await _beatScheduler.Pause();

            var online = (state != PlaybackState.Disconnected && state != PlaybackState.Failed);
            
            // TODO: Include time
            InvokeOnMainThread(() =>
            {
                PlayerStatus.Cell.DoubleValue = online ? 1.0 : 0.0;
                PlayerState.StringValue = state.ToString();
            });
        }

        private async void LatencyUpdate(bool connected, ExponentialMovingAverage latency, double mean)
        {
            _beatScheduler.Latency = TimeSpan.FromMilliseconds(mean);

            InvokeOnMainThread(() =>
            {
                BeatDetectorStatus.Cell.DoubleValue = connected ? 1.0 : 0.0;
                BeatDetectorLatency.StringValue = connected ? $"{mean:0.00} ms latency" : "Not responding";
            });
        }

        private async void SettingsChanged(object sender, EventArgs e)
        {
            var settings = AppDelegate.Settings;

            if (settings.BaseUrl != _connectionManager.BaseUrl)
            {
                await _connectionManager.Stop();
                await _connectionManager.Start(settings.BaseUrl);
            }

            if ((settings.MidiDevice ?? string.Empty) != (_midiInterface.Name ?? string.Empty))
            {
                await _midiInterface.Disconnect();
                await _midiInterface.Connect(settings.MidiDevice);
            }
        }

        private async Task Disconnect()
        {
            await _connectionManager?.Stop();
        }

        private void ResetStatus()
        {
            InvokeOnMainThread(() =>
            {
                ConnectionStatus.Cell.DoubleValue = 0.0;
                PlayerStatus.Cell.DoubleValue = 0.0;
                BeatDetectorStatus.Cell.DoubleValue = 0.0;
                BeatStatus.Cell.DoubleValue = 0.0;
                MidiStatus.Cell.DoubleValue = 0.0;
            });
        }

        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }
    }
}
