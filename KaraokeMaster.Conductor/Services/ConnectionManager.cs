﻿/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace KaraokeMaster.Conductor
{
    public class ConnectionManager : IDisposable
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly BeatScheduler _beatScheduler;
        private readonly PlayerStateMonitor _playerStateMonitor;
        private readonly LatencyMonitor _latencyMonitor;
        private readonly LightingCueMonitor _lightingCueMonitor;
        private readonly WebhookMonitor _webhookMonitor;

        private HubConnection _connection;
        private HubConnection _playerConnection;

        public string BaseUrl { get; private set; }
        public bool Connected { get; private set; }

        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;
        public event BeatReceivedEventArgs OnBeatReceived;

        public delegate void BeatReceivedEventArgs(double tempo);

        public ConnectionManager(BeatScheduler beatScheduler, PlayerStateMonitor playerStateMonitor, LatencyMonitor latencyMonitor, LightingCueMonitor lightingCueMonitor, WebhookMonitor webhookMonitor)
        {
            _beatScheduler = beatScheduler;
            _playerStateMonitor = playerStateMonitor;
            _latencyMonitor = latencyMonitor;
            _lightingCueMonitor = lightingCueMonitor;
            _webhookMonitor = webhookMonitor;

            Connected = false;
        }

        public async Task Start(string baseUrl)
        {
            BaseUrl = baseUrl;

            Uri url;
            Uri playerUrl;

            try
            {
                url = new Uri(new Uri(BaseUrl), "api/hub");
                if (url.Host.StartsWith("localhost"))
                    url = new Uri(new Uri(BaseUrl), "hub");

                playerUrl = new Uri(new Uri(BaseUrl), "api/hubs/player");
                if (playerUrl.Host.StartsWith("localhost"))
                    playerUrl = new Uri(new Uri(BaseUrl), "hubs/player");
            }
            catch (Exception ex)
            {
                Log.Error("Unable to parse URL", ex);
                return;
            }

            Log.Info($"Connecting to KaraokeMaster API {url}");

            await Task.Run(async () =>
            {
                _connection = new HubConnectionBuilder()
                    .WithUrl(url)
                    .WithAutomaticReconnect(new RetryPolicy())
                    .Build();

                try
                {
                    await _connection.StartAsync();
                    Log.Info("Connected!");
                    OnConnect?.Invoke(this, null);
                }
                catch (Exception ex)
                {
                    // TODO: Tell the user?
                    Log.Error($"Failed to connect to {url}", ex);
                    OnDisconnect?.Invoke(this, null);
                    return;
                }
            });

            Log.Info($"Connecting to KaraokeMaster Player API {url}");

            await Task.Run(async () =>
            {
                _playerConnection = new HubConnectionBuilder()
                    .WithUrl(playerUrl)
                    .WithAutomaticReconnect(new RetryPolicy())
                    .Build();

                try
                {
                    await _playerConnection.StartAsync();
                    Log.Info("Player Connected!");
                    // OnConnect?.Invoke(this, null);
                }
                catch (Exception ex)
                {
                    // TODO: Tell the user?
                    Log.Error($"Failed to connect to {playerUrl}", ex);
                    // OnDisconnect?.Invoke(this, null);
                    return;
                }
            });

            // TODO: Consolidate hub connection states/events

            await _playerStateMonitor.Start(_playerConnection);
            await _latencyMonitor.Start(_connection);
            await _lightingCueMonitor.Start(_connection);
            await _webhookMonitor.Start(_connection);

            _connection.On<double>("Beat", BeatReceived);

            _connection.Reconnecting += HubReconnecting;
            _connection.Reconnected += HubReconnected;
            _connection.Closed += HubDisconnected;

            _playerConnection.Reconnecting += PlayerHubReconnecting;
            _playerConnection.Reconnected += PlayerHubReconnected;
            _playerConnection.Closed += PlayerHubDisconnected;
        }

        public async Task Stop()
        {
            try
            {
                _connection?.Remove("Beat");

                if (_playerStateMonitor != null)
                    await _playerStateMonitor.Stop();
                if (_latencyMonitor != null)
                    await _latencyMonitor.Stop();
                if (_lightingCueMonitor != null)
                    await _lightingCueMonitor.Stop();
                if (_webhookMonitor != null)
                    await _webhookMonitor.Stop();

                if (_connection != null)
                    await _connection.DisposeAsync();
            }
            catch { }

            _connection = null;
        }

        public async void Dispose()
        {
            await Stop();
        }

        private async Task BeatReceived(double tempo)
        {
#if DEBUG
            Log.Debug($"BEAT {tempo:0}");
#endif

            _beatScheduler.Beat(tempo);

            OnBeatReceived?.Invoke(tempo);
        }

        private async Task HubReconnecting(Exception arg)
        {
            Log.Debug("Hub reconnecting...");
        }

        private async Task HubReconnected(string arg)
        {
            Log.Info("Hub reconnected");
        }

        private async Task HubDisconnected(Exception arg)
        {
            Log.Info("Hub disconnected");
        }

        private async Task PlayerHubReconnecting(Exception arg)
        {
            Log.Debug("Player hub reconnecting...");
        }

        private async Task PlayerHubReconnected(string arg)
        {
            Log.Info("Player hub reconnected");
        }

        private async Task PlayerHubDisconnected(Exception arg)
        {
            Log.Info("Player hub disconnected");
        }

        private class RetryPolicy : IRetryPolicy
        {
            private static readonly TimeSpan OneMinute = TimeSpan.FromMinutes(1);
            private static readonly TimeSpan FifteenSeconds = TimeSpan.FromSeconds(15);

            public TimeSpan? NextRetryDelay(RetryContext retryContext)
            {
                // TODO: Find a way to be even smarter-er with how often we retry
                // TODO: Perhaps query module twin, look for mDNS broadcast, etc.?
                if (retryContext.ElapsedTime > OneMinute)
                    return OneMinute;

                return FifteenSeconds;
            }
        }
    }
}
