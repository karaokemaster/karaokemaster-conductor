﻿/*
    
    Copyright 2020-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using KaraokeMaster.Conductor.Models;
using Microsoft.AspNetCore.SignalR.Client;

namespace KaraokeMaster.Conductor
{
    public class WebhookMonitor
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly PlayerStateMonitor _playerStateMonitor;
        private HubConnection _connection;
        private Timer _idleTimer;

        public WebhookMonitor(PlayerStateMonitor playerStateMonitor)
        {
            _playerStateMonitor = playerStateMonitor;

            _playerStateMonitor.OnUpdate += PlayerStateChanged;
        }

        public async Task Start(HubConnection connection)
        {
            _connection = connection;

            _connection.Closed += ConnectionClosed;
            AppDelegate.Settings.OnUpdate += Settings_OnUpdate;
        }

        public async Task Stop()
        {
            AppDelegate.Settings.OnUpdate -= Settings_OnUpdate;

            if (_idleTimer != null)
                await _idleTimer.DisposeAsync();

            _idleTimer = null;
            _connection = null;
        }

        public async void Dispose()
        {
            await Stop();
        }

        private async Task ConnectionClosed(Exception arg)
        {
            await Stop();
        }

        private async void Settings_OnUpdate(object sender, EventArgs e)
        {
            // TODO: Maybe something? Maybe not?
        }

        private async void PlayerStateChanged(PlaybackState state)
        {
            // TODO: Remove duplicated logic, combine into single web call
            var playing = (state == PlaybackState.Starting || state == PlaybackState.Playing || state == PlaybackState.StoppingAfter);

            if (playing && !string.IsNullOrEmpty(AppDelegate.Settings.PlaybackStartWebhook))
            {
                var url = AppDelegate.Settings.PlaybackStartWebhook;
                var method = AppDelegate.Settings.PlaybackStartWebhookMethod ?? "GET";

                Log.Info($"Webhook: playback started");
                Log.Debug($"Webhook URL: {url} ({method})");

                _idleTimer?.Dispose();
                _idleTimer = null;

                try
                {
#if !DEBUG
                    var request = WebRequest.Create(url);
                    request.Method = method;
                    request.Timeout = 2500;
                    var response = await request.GetResponseAsync();

                    Log.Debug($"{url} status code {(response as HttpWebResponse).StatusCode}");

                    response.Close();
#endif
                }
                catch (Exception ex)
                {
                    Log.Warn("Playback start webhook failed", ex);
                }
            }
            else if (!playing && !string.IsNullOrEmpty(AppDelegate.Settings.PlaybackStopWebhook))
            {
                var timeout = TimeSpan.FromSeconds(30);
                Log.Debug($"Player Idle Webhook: scheduling in {timeout}");

                _idleTimer?.Dispose();
                _idleTimer = null;

                _idleTimer = new Timer(async timerState =>
                {
                    var url = AppDelegate.Settings.PlaybackStopWebhook;
                    var method = AppDelegate.Settings.PlaybackStopWebhookMethod ?? "GET";

                    Log.Info($"Webhook: player idle");
                    Log.Debug($"Webhook URL: {url} ({method})");

                    if (_playerStateMonitor.Playing)
                    {
                        Log.Info($"Player Idle Webhook: player state does not match - aborting");

                        _idleTimer?.Dispose();
                        _idleTimer = null;

                        return;
                    }

                    try
                    {
#if !DEBUG
                        var request = WebRequest.Create(url);
                        request.Method = method;
                        request.Timeout = 2500;
                        var response = await request.GetResponseAsync();

                        Log.Debug($"{url} status code {(response as HttpWebResponse).StatusCode}");

                        response.Close();
#endif
                    }
                    catch (Exception ex)
                    {
                        Log.Warn("Playback stop webhook failed", ex);
                    }

                    _idleTimer?.Dispose();
                    _idleTimer = null;
                }, null, timeout, TimeSpan.Zero);
            }
        }
    }
}
