﻿/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace KaraokeMaster.Conductor
{
    public class LatencyMonitor : IDisposable
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private HubConnection _connection;
        private Stopwatch _latencyStopwatch;
        private Timer _latencyTimer;

        // TODO: Use a separate event for connect/disconnect
        public event LatencyUpdateEventArgs OnUpdate;
        public delegate void LatencyUpdateEventArgs(bool connected, ExponentialMovingAverage latency, double mean);

        public LatencyMonitor()
        {
        }

        public async Task Start(HubConnection connection)
        {
            _connection = connection;

            _connection.On<long, bool>("BeatEcho", EchoReceived);
            _connection.Closed += ConnectionClosed;

            await CheckLatency();
        }

        public async Task Stop()
        {
            _connection?.Remove("BeatEcho");

            if (_latencyTimer != null)
                await _latencyTimer.DisposeAsync();

            _latencyStopwatch?.Stop();

            _connection = null;
            _latencyStopwatch = null;
            _latencyTimer = null;
            Latency = null;
        }

        public ExponentialMovingAverage Latency { get; private set; }

        public async void Dispose()
        {
            await Stop();
        }

        private async Task ConnectionClosed(Exception arg)
        {
            await Stop();
        }

        private async Task EchoReceived(long thenTimestamp, bool alive)
        {
            _latencyStopwatch?.Stop();

            _latencyTimer?.Dispose();
            _latencyTimer = null;

            if (alive)
            {
                // We should only ever be sending echoes with alive == true...
                Log.Warn($"WHY TF ARE WE GETTING ANOTHER BEAT ECHO?! {thenTimestamp}");
                return;
            }

            var nowTimestamp = Stopwatch.GetTimestamp();
            var roundTripTicks = (_latencyStopwatch?.ElapsedTicks ?? long.MaxValue) / TimeSpan.TicksPerMillisecond;
            var roundTripTime = _latencyStopwatch?.ElapsedMilliseconds ?? long.MaxValue;

            if (Latency == null)
                Latency = new ExponentialMovingAverage(0.3, roundTripTicks);
            else
                Latency.Update(roundTripTicks);

            OnUpdate?.Invoke(true, Latency, Latency.Mean);

            Log.Debug($"Received BeatEcho response with timestamp {thenTimestamp} (elapsed round trip time {roundTripTime} ms; now timestamp {nowTimestamp})");
            Log.Info($"BeatEcho RTT {roundTripTicks:0.00} ms; average {Latency.Mean:0.00}; std.dev. {Latency.StdDev:0.00}");

            if (_latencyTimer != null)
            {
                // The timer should get destroyed before the echo returns, so this should never be true...
                Log.Warn($"Latency timer still active, check all the things.");
                return;
            }

            var recheckIn = TimeSpan.FromSeconds(15); // TimeSpan.FromMinutes(1);
                                                      // If this latency >> avg, check sooner?
                                                      // If variance/stddev is high then check sooner?
                                                      // Maybe by default check less often, though what's the harm?
            _latencyTimer = new Timer(async state =>
            {
                await CheckLatency();
                _latencyTimer?.DisposeAsync();
                _latencyTimer = null;
            }, null, recheckIn, TimeSpan.FromMilliseconds(-1));
        }

        private async Task CheckLatency()
        {
            if (_connection.State == HubConnectionState.Connected)
            {
                Log.Info($"Initiating beat detection network latency check...");

                _latencyStopwatch = new Stopwatch();
                var timestamp = Stopwatch.GetTimestamp();

                try
                {
                    _latencyStopwatch.Start();
                    await _connection.InvokeAsync("BeatEcho", timestamp, true);

                    Log.Debug($"Sent BeatEcho request with timestamp {timestamp} (elapsed call time {_latencyStopwatch.ElapsedMilliseconds} ms)");
                }
                catch (Exception ex)
                {
                    Log.Warn("Error sending beat echo", ex);
                }
            }

            _latencyTimer = new Timer(state =>
            {
                Log.Warn($"BeatEcho not received after 15 seconds!");
                // TODO: Use a separate event
                OnUpdate?.Invoke(false, null, 0);
                CheckLatency();
            }, null, TimeSpan.FromSeconds(15), TimeSpan.FromMilliseconds(-1));
        }
    }
}
