﻿/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using Zeroconf;

namespace KaraokeMaster.Conductor.Services
{
    public class ApiDiscoveryService : IDisposable
    {
        private const string Protocol = "_karaokemaster._tcp.local.";

        private readonly ObservableCollection<string> _instances;
        private readonly List<IDisposable> _disposables;

        public ApiDiscoveryService()
        {
            _instances = new ObservableCollection<string>();
            _disposables = new List<IDisposable>();

            Start();
        }

        public void Dispose()
        {
            Stop();
        }

        public List<string> Instances => _instances.ToList();

        public Action OnUpdate { get; set; }

        private void Start()
        {
            Console.WriteLine("Starting API Discovery...");

            _disposables
                .Add(ZeroconfResolver.ResolveContinuous(Protocol)
                    //.ObserveOn(SynchronizationContext.Current)
                    .Subscribe(HostFound)
                );
        }

        private void Stop()
        {
            foreach (var disposable in _disposables)
            {
                try
                {
                    disposable.Dispose();
                }
                catch { }
            }
        }

        private void HostFound(IZeroconfHost host)
        {
            var service = host.Services.FirstOrDefault(s => s.Value.Name == Protocol).Value;
            var serviceUrl = service.Properties[0]["url"];

            Console.WriteLine($"Host found: {host.DisplayName} {serviceUrl}");

            // TODO: Use something more specific as the instance ID
            if (_instances.Any(i => i == serviceUrl))
            {
                //var ipChangedVirb = Cameras.SingleOrDefault(v => v.Name == host.DisplayName && v.Ip != host.IPAddress);
                //if (ipChangedVirb == null)
                    return;
                // RemoveInstance(i);
            }

            // TODO: Store more params, so we can have a friendly display name, etc.
            _instances.Add(serviceUrl);
            
            OnUpdate?.Invoke();
        }
    }
}
