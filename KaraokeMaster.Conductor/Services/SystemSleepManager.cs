﻿/*
    
    Copyright 2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Runtime.InteropServices;
using CoreFoundation;
using Foundation;

namespace KaraokeMaster.Conductor
{
    public class SystemSleepManager : IDisposable
    {
        [DllImport("/System/Library/Frameworks/IOKit.framework/IOKit")]
        private static extern int IOPMAssertionCreateWithName(IntPtr assertionType, int assertionLevel, IntPtr assertionName, out int assertionID);

        [DllImport("/System/Library/Frameworks/IOKit.framework/IOKit")]
        private static extern int IOPMAssertionCreateWithProperties(IntPtr /* Pass NSDictionary.Handle */ prop, ref int assertionID);

        [DllImport("/System/Library/Frameworks/IOKit.framework/IOKit")]
        private static extern int IOPMAssertionRelease(int assertionID);

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private bool _disableDisplaySleep;
        private bool _disableAppNap;
        private int _assertionId = 0;
        private NSObject _activity;

        public SystemSleepManager(bool disableDisplaySleep = true, bool disableAppNap = true)
        {
            _disableDisplaySleep = disableDisplaySleep;
            _disableAppNap = disableAppNap;
        }

        public void DisableSleep()
        {
            if (_disableDisplaySleep && _assertionId == 0)
            {
                Log.Debug("Disabling display sleep...");

                //var props = new NSDictionary();
                //props.SetValueForKey(new NSString("NoDisplaySleepAssertion"), new NSString("AssertType"));
                //props.SetValueForKey(new NSString(""), new NSString("AssertValue")); // kIOPMAssertionValueKey
                //props.SetValueForKey(new NSString("KaraokeMaster Conductor"), new NSString("AssertName"));
                //props.SetValueForKey(new NSNumber(255), new NSString("AssertLevel"));
                //props.SetValueForKey(new NSString(""), new NSString(""));

                var assertType = new CFString("NoDisplaySleepAssertion");
                var assertName = new CFString("Watching Clip");
                var returnError = IOPMAssertionCreateWithName(assertType.Handle, 255, assertName.Handle, out int assertionID);

                if (returnError != 0)
                    Log.Warn($"Error disabling display sleep: {returnError:X}");

                _assertionId = assertionID;
            }

            if (_disableAppNap && _activity == null)
            {
                Log.Debug("Disabling AppNap...");

                // Prevent auto-sleeping
                var reason = "KaraokeMaster Conductor";

                _activity = new NSProcessInfo().BeginActivity(NSActivityOptions.IdleSystemSleepDisabled | NSActivityOptions.SuddenTerminationDisabled, reason);
            }
        }

        public void EnableSleep()
        {
            if (_assertionId != 0)
            {
                Log.Debug("Enabling display sleep...");

                var returnError = IOPMAssertionRelease(_assertionId);

                if (returnError != 0)
                    Log.Warn($"Error enabling display sleep: {returnError:X}");

                _assertionId = 0;
            }

            if (_activity != null)
            {
                Log.Debug("Enabling AppNap...");

                // End the battery draining activty
                new NSProcessInfo().EndActivity(_activity);
            }
        }

        public void Dispose()
        {
            EnableSleep();
        }
    }
}
