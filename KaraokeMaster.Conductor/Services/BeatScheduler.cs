﻿/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace KaraokeMaster.Conductor
{
    public class BeatScheduler : IDisposable
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly float _tickFrequency = 1000f / Stopwatch.Frequency;

        private readonly TimeSpan _oneMinute = TimeSpan.FromMinutes(1);

        private Thread _beatThread;
        private readonly Action _clockHandler;
        private ManualResetEvent _stop = new ManualResetEvent(false);
        private readonly int _clocksPerBeat;

        private volatile float _interval;
        private volatile float _offset;
        private volatile float _maxOffset;
        private volatile bool _isRunning;
        private volatile int _clock = 0;

        // TODO: Add configuration options
        private readonly TimeSpan _baseLatency = TimeSpan.FromMilliseconds(10);

        private DateTime _nextBeat = DateTime.MinValue;
        private double _tempo = 0.0;
        private TimeSpan _beatInterval;

        public BeatScheduler(Action clockHandler, int ticksPerBeat)
        {
            _clockHandler = clockHandler;
            _clocksPerBeat = ticksPerBeat;

            _beatThread = new Thread(BeatLoop);
            _beatThread.Priority = ThreadPriority.Highest;
        }

        public void Dispose()
        {
            _stop?.Set();
            _beatThread?.Join();

            _beatThread = null;
            _stop = null;
        }

        public TimeSpan Latency { get; set; }

        private void SetTempo(double value)
        {
            if (_tempo == value) return;

            _tempo = value;
            while (_tempo > 170) _tempo /= 2;
            while (_tempo < 70) _tempo *= 2;

            _beatInterval = _oneMinute / _tempo;
            _interval = _beatInterval.Ticks / _clocksPerBeat * _tickFrequency;
            _maxOffset = _interval / 120;

            Log.Debug($"Tempo changed to {_tempo} (adjusted from {value}); tick interval {_interval:0.000} ms, max offset {_maxOffset:0.000} ms");
        }

        public async Task Beat(double tempo)
        {
            if (!_isRunning)
            {
                _isRunning = true;
                _nextBeat = DateTime.Now;
                _beatThread.Start();
            }

            // Calculate "actual" beat time, and set the tempo to get the interval between beats
            // TODO: Temper sudden BPM changes/out-of-range values
            SetTempo(tempo);
            var totalLatency = Latency - _baseLatency;
            var beatTime = DateTime.Now - totalLatency;

            // Calculate when the next beat "should" be due
            var nextBeat = beatTime + _beatInterval;
            while (nextBeat < DateTime.Now) nextBeat += _beatInterval;

            // Compare to next beat due
            var offset = nextBeat - _nextBeat;
            _offset = Math.Min(Math.Abs(offset.Ticks / (_clocksPerBeat - _clock) * _tickFrequency), _maxOffset) * Math.Sign(offset.Ticks);
            _nextBeat = nextBeat;

            Log.Debug($"Offset from current schedule {offset:c}; applied tick offset {_offset:0.000} ms; TickInterval {_interval:0.000} ms");
        }

        private void BeatLoop()
        {
            // Inspired by https://stackoverflow.com/a/41697139
            Log.Debug($"Stopwatch.IsHighResolution: {Stopwatch.IsHighResolution}");

            var nextTrigger = 0f;
            var s = new Stopwatch();
            s.Start();

            while (_isRunning)
            {
                nextTrigger += _interval - _offset;
                float elapsed;

                while (true)
                {
                    elapsed = s.ElapsedTicks * _tickFrequency;
                    var diff = nextTrigger - elapsed;
                    if (diff <= 0f) break;

                    if (diff < 1f)
                        Thread.SpinWait(10);
                    else if (diff < 5f)
                        Thread.SpinWait(100);
                    else if (diff < 15f)
                        Thread.Sleep(1);
                    else
                        Thread.Sleep(10);

                    if (!_isRunning)
                        return;
                }

                // float delay = elapsed - nextTrigger;

                _clock = (_clock + 1) % _clocksPerBeat;
                if (_clock == 0)
                {
                    _nextBeat += _beatInterval;
                    _offset = 0;
                }

                _clockHandler.Invoke();

                if (s.Elapsed.TotalHours >= 1d)
                {
                    s.Restart();
                    nextTrigger = 0f;
                }
            }

            s.Stop();
        }

        public async Task Pause()
        {
            // _beatTimer.Change(TimeSpan.FromMilliseconds(-1), TimeSpan.Zero);
        }

    }
}