﻿/*
    
    Copyright 2020 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Threading;
using System.Threading.Tasks;
using KaraokeMaster.Conductor.Models;
using MediaPlayer;
using Microsoft.AspNetCore.SignalR.Client;

namespace KaraokeMaster.Conductor
{
    public class PlayerStateMonitor : IDisposable
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private HubConnection _connection;
        private Timer _statusTimer;
        private PlaybackState _lastState;
        private CancellationTokenSource _cancellationTokenSource;
        private Thread _stateChangeThread;
        private readonly TimeSpan _timeout = TimeSpan.FromSeconds(15);

        public event PlayerStateEventArgs OnUpdate;
        public delegate void PlayerStateEventArgs(PlaybackState state);

        public PlayerStateMonitor()
        {
            _lastState = PlaybackState.Failed;
        }

        public bool Online { get; private set; } = false;
        public bool Playing { get; private set; } = false;

        public async Task Start(HubConnection connection)
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _connection = connection;

            _connection.Closed += ConnectionClosed;
            _connection.Reconnecting += ConnectionReconnecting;
            _connection.Reconnected += ConnectionRestored;

            StreamPlayerState();
        }

        public async Task Stop()
        {
            _cancellationTokenSource?.Cancel();

            if (_statusTimer != null)
                await _statusTimer.DisposeAsync();

            OnUpdate?.Invoke(PlaybackState.Disconnected);

            _statusTimer = null;
        }

        private async Task StreamPlayerState()
        {
            // TODO: Also track playback time/completion
            var channel = await _connection.StreamAsChannelAsync<PlayerState>("SubscribePlayerState", _cancellationTokenSource.Token);

            _ = Task.Run(async () =>
            {
                while (await channel.WaitToReadAsync())
                {
                    // Read all currently available data synchronously, before waiting for more data
                    while (channel.TryRead(out var state))
                    {
                        ResetStatusTimeout();

                        var npi = MPNowPlayingInfoCenter.DefaultCenter.NowPlaying;
                        npi.PlaybackDuration = (double?)state.VideoDuration;
                        npi.ElapsedPlaybackTime = (double?)state.VideoTime;
                        npi.DefaultPlaybackRate = 1.0;
                        npi.PlaybackRate = 1.0;
                        MPNowPlayingInfoCenter.DefaultCenter.NowPlaying = npi;

                        if (_lastState == state.State) continue;

                        Log.Debug($"Player state changed: {state.State}");
                        try
                        {
                            if (_stateChangeThread.ThreadState != ThreadState.Stopped)
                                _stateChangeThread.Abort();
                            _stateChangeThread = null;
                        }
                        catch { }

                        _lastState = state.State;
                        Playing = _lastState == PlaybackState.Playing || _lastState == PlaybackState.StoppingAfter;

                        _stateChangeThread = new Thread(() =>
                        {
                            Thread.CurrentThread.IsBackground = true;

                            try
                            {
                                OnUpdate?.Invoke(state.State);
                            }
                            catch (ThreadAbortException) { }
                            catch (Exception ex)
                            {
                                Log.Warn("Error in state change handler thread", ex);
                            }
                        });
                        _stateChangeThread.Start();

                        // TODO: More thorough state communication
                        MPNowPlayingInfoCenter.DefaultCenter.PlaybackState = Playing ? MPNowPlayingPlaybackState.Playing : MPNowPlayingPlaybackState.Stopped;
                    }
                }
            }, _cancellationTokenSource.Token);
        }

        private async Task ConnectionReconnecting(Exception arg)
        {
            Log.Debug($"Player state connection lost");

            await Stop();
        }

        private async Task ConnectionRestored(string arg)
        {
            Log.Debug($"Player state connection restored");

            StreamPlayerState();
        }

        private async Task ConnectionClosed(Exception arg)
        {
            Log.Debug($"Player state connection closed");

            await Stop();
            _connection = null;

            MPNowPlayingInfoCenter.DefaultCenter.PlaybackState = MPNowPlayingPlaybackState.Unknown;
        }

        private void ResetStatusTimeout()
        {
            _statusTimer?.Dispose();
            _statusTimer = null;

            _statusTimer = new Timer(state =>
            {
                Log.Info($"Player status timeout expired");

                _lastState = PlaybackState.Disconnected;
                OnUpdate?.Invoke(PlaybackState.Disconnected);

                _statusTimer?.Dispose();
            }, null, TimeSpan.FromMilliseconds(-1), _timeout);
        }

        public async void Dispose()
        {
            await Stop();
        }
    }
}
