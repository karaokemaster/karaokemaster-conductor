﻿/*
    
    Copyright 2020-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using KaraokeMaster.Conductor.Models;
using Microsoft.AspNetCore.SignalR.Client;

namespace KaraokeMaster.Conductor
{
    public class LightingCueMonitor : IDisposable
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly MidiInterface _midiInterface;
        private readonly PlayerStateMonitor _playerStateMonitor;
        private HubConnection _connection;
        private Timer _idleTimer;
        private Timer _cueStateTimer;

        public LightingCueMonitor(MidiInterface midiInterface, PlayerStateMonitor playerStateMonitor)
        {
            _midiInterface = midiInterface;
            _playerStateMonitor = playerStateMonitor;

            _midiInterface.OnConnect += MidiInterfaceConnected;
            _midiInterface.OnCueStateChange += CueStateChanged;
            _playerStateMonitor.OnUpdate += PlayerStateChanged;
        }

        public async Task Start(HubConnection connection)
        {
            _connection = connection;

            // TODO: We should make sure we're filtering out "special" entries (e.g. 0 and 1)
            _connection.On<int>("SendCue", SendCue);
            _connection.On("RequestCues", SendCueList);

            _connection.Closed += ConnectionClosed;
            AppDelegate.Settings.OnUpdate += Settings_OnUpdate;
            AppDelegate.Settings.OnTestCue += Settings_OnTestCue;

            SendInitialCues();
            await SendCueList();
        }

        public async Task Stop()
        {
            AppDelegate.Settings.OnUpdate -= Settings_OnUpdate;
            AppDelegate.Settings.OnTestCue -= Settings_OnTestCue;
            _connection?.Remove("SendCue");
            _connection?.Remove("RequestCues");

            if (_midiInterface != null && _midiInterface.Connected)
                await _midiInterface.SendCue(0);

            if (_idleTimer != null)
                await _idleTimer.DisposeAsync();

            _idleTimer = null;
            _connection = null;
        }

        public async void Dispose()
        {
            await Stop();
        }

        private async Task ConnectionClosed(Exception arg)
        {
            await Stop();
        }

        private async void Settings_OnUpdate(object sender, EventArgs e)
        {
            await SendCueList();
        }

        private async void Settings_OnTestCue(Cue cue)
        {
            if (!_midiInterface.Connected) return;

            await _midiInterface.SendCue(cue.Index);
        }

        private async Task SendCue(int index)
        {
            if (_midiInterface == null || !_midiInterface.Connected) return;

            await _midiInterface.SendCue(index);
        }

        private async Task SendCueList()
        {
            if (_connection == null || _connection.State != HubConnectionState.Connected) return;

            var cues = AppDelegate.Settings.Cues.ToArray();

            try
            {
                await _connection.InvokeAsync<Cue[]>("UpdateCues", cues);
            }
            catch (Exception ex)
            {
                Log.Warn("Error sending cue list update", ex);
            }
        }

        private async void PlayerStateChanged(PlaybackState state)
        {
            if (_midiInterface == null || !_midiInterface.Connected) return;

            // TODO: On first transfer to playing state, trigger first configured cue
            if (state == PlaybackState.Playing || state == PlaybackState.StoppingAfter)
            {
                _idleTimer?.Dispose();
                _idleTimer = null;

                Log.Info($"Lighting cue: Playing");
                await _midiInterface.SendCue(2);
            }
            else if (state == PlaybackState.Starting)
            {
                _idleTimer?.Dispose();
                _idleTimer = null;

                Log.Info($"Lighting cue: Starting");
                await _midiInterface.SendCue(1);
            }
            else
            {
                _idleTimer?.Dispose();
                _idleTimer = null;

                _idleTimer = new Timer(async timerState =>
                {
                    if (_playerStateMonitor.Playing)
                    {
                        Log.Info($"Lighting cue: Idle - player state does not match - aborting");

                        _idleTimer?.Dispose();
                        _idleTimer = null;

                        return;
                    }

                    Log.Info($"Lighting cue: Idle");
                    await _midiInterface.SendCue(0);

                    _idleTimer?.Dispose();
                    _idleTimer = null;

                    _idleTimer = new Timer(async blackoutTimerState =>
                    {
                        if (_playerStateMonitor.Playing)
                        {
                            Log.Info($"Lighting cue: Blackout - player state does not match - aborting");

                            _idleTimer?.Dispose();
                            _idleTimer = null;

                            return;
                        }

                        Log.Info($"Lighting cue: Blackout");
                        await _midiInterface.SendCue(127);

                        _idleTimer?.Dispose();
                        _idleTimer = null;
                    }, null, TimeSpan.FromSeconds(60), TimeSpan.Zero);
                }, null, TimeSpan.FromSeconds(2), TimeSpan.Zero);
            }
        }

        private async void CueStateChanged(int index, bool state)
        {
            var cue = AppDelegate.Settings.Cues.FirstOrDefault(c => c.Index == index);

            if (cue == null) return;
            _cueStateTimer?.Dispose();
            _cueStateTimer = null;

            cue.State = state ? CueState.On : CueState.Off;

            Log.Debug($"Cue {cue.Name} state changed to {cue.State}");

            _cueStateTimer = new Timer(async timerState =>
            {
                Log.Info($"Sending updated cue states");

                await SendCueList();

                _cueStateTimer?.Dispose();
                _cueStateTimer = null;
            }, null, TimeSpan.FromMilliseconds(250), TimeSpan.Zero);
        }

        private async void SendInitialCues()
        {
            if (!_midiInterface.Connected) return;

            if (_playerStateMonitor.Playing)
                await _midiInterface.SendCue(2);
            else
                await _midiInterface.SendCue(0);

            var cue = AppDelegate.Settings.Cues.FirstOrDefault(c => c.Theme);
            if (cue != null)
                await _midiInterface.SendCue(cue.Index);
        }

        private void MidiInterfaceConnected(object sender, EventArgs e)
        {
            SendInitialCues();
        }
    }
}
