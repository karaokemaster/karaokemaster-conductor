﻿/*
    
    Copyright 2020-2021 Mike Peschka

    This file is part of KaraokeMaster.
  
    KaraokeMaster is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    KaraokeMaster is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with KaraokeMaster.  If not, see <https://www.gnu.org/licenses/>.
  
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using AppKit;
using CoreMidi;

namespace KaraokeMaster.Conductor
{
    public class MidiInterface : IDisposable
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        const byte MIDI_BEAT_START = 0xFA;
        const byte MIDI_BEAT_CLOCK = 0xF8;
        const byte MIDI_BEAT_CONTINUE = 0xFB;
        const byte MIDI_BEAT_STOP = 0xFC;
        const byte MIDI_NOTE_ON = 0x90; // May be 0x90 through 0x9F for different channels
        const byte MIDI_NOTE_OFF = 0x80; // May be 0x80 through 0x8F for different channels
        const byte MIDI_CONTINUOUS_CONTROLLER = 0xB0;

        private readonly MidiPacket _tickPacket = MakePacket(MIDI_BEAT_CLOCK);
        private MidiClient _client;
        private MidiEndpoint _endpoint;
        private MidiEndpoint _feedbackEndpoint;
        private MidiPort _port;
        private CancellationTokenSource _tokenSource;

        public delegate void CueStateEventArgs(int index, bool state);

        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;
        public event CueStateEventArgs OnCueStateChange;
        public string Name { get; private set; }
        public bool Connected { get; private set; }

        public static Dictionary<string, string> Devices
        {
            get
            {
                var devices = new Dictionary<string, string>();

                NSApplication.SharedApplication.InvokeOnMainThread(() =>
                {
                    Console.WriteLine($"Destination count {Midi.DestinationCount}");

                    for (nint i = 0; i < Midi.DestinationCount; i++)
                    {
                        var dest = MidiEndpoint.GetDestination(i);
                        devices.Add(dest.EndpointName, dest.Name);
                        Console.WriteLine($"- {dest.Name} {dest.EndpointName}");
                    }
                });

                return devices;
            }
        }

        public MidiInterface()
        {
        }

        public async Task Connect() => await Connect(null);

        public async Task Connect(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                Name = null;
                await SetupSource();
            }
            else
            {
                _tokenSource = new CancellationTokenSource();
                Name = name;
                await SetupDestination(name);
            }
        }

        public async Task Disconnect()
        {
            var stopPacket = MakePacket(MIDI_BEAT_STOP);
            _endpoint?.Received(new[] { stopPacket });

            _tokenSource?.Cancel();

            _port?.Dispose();
            _endpoint?.Dispose();
            _client?.Dispose();

            Connected = false;
            Name = null;
        }

        public async void Dispose()
        {
            await Disconnect();
        }

        private async Task SetupSource()
        {
            NSApplication.SharedApplication.InvokeOnMainThread(() =>
            {
                Console.WriteLine($"Destination count {CoreMidi.Midi.DestinationCount}");
                for (int i = 0; i < CoreMidi.Midi.DestinationCount; i++)
                {
                    var dest = MidiEndpoint.GetDestination(i);
                    Console.WriteLine($"- {dest.Name} {dest.EndpointName}");
                }

                Log.Info("Creating KaraokeMaster MIDI source...");

                _client = new MidiClient("KaraokeMaster");
                _endpoint = _client.CreateVirtualSource("KaraokeMaster", out var err);
                _endpoint.ConnectionUniqueIDInt = 31337;

                _feedbackEndpoint = _client.CreateVirtualDestination("KaraokeMaster", out var err2);
                _feedbackEndpoint.ConnectionUniqueIDInt = 31337;
                _feedbackEndpoint.MessageReceived += FeedbackEndpointReceived;
            });

            Connected = true;

            await SendStart();
        }

        private async Task SetupDestination(string name)
        {
            bool found = false;

            while (!found)
            {
                Log.Debug($"Looking for MIDI destination {name}...");
                NSApplication.SharedApplication.InvokeOnMainThread(() =>
                {
                    for (int i = 0; i < CoreMidi.Midi.DestinationCount; i++)
                    {
                        var dest = MidiEndpoint.GetDestination(i);

                        if (dest.Name != name) continue;

                        Log.Debug($"MIDI destination {name} found!");
                        _endpoint = dest;
                        found = true;
                        break;
                    }
                });

                if (!found)
                {
                    Log.Debug($"MIDI destination {name} not found; retrying in 5s...");
                    await Task.Delay(5000, _tokenSource.Token);
                }
            }

            _client = new MidiClient("KaraokeMaster");
            _port = _client.CreateOutputPort("beat");
            _port.ConnectSource(_endpoint);

            Connected = true;
            Log.Info($"MIDI destination {name} connected");
            await SendStart();
        }

        private void FeedbackEndpointReceived(object sender, MidiPacketsEventArgs e)
        {
            e.Packets.ToList().ForEach(p =>
            {
                var packetBytes = new byte[p.Length];
                Marshal.Copy(p.Bytes, packetBytes, 0, p.Length);

                int cueIndex;

                switch (packetBytes[0])
                {
                    case MIDI_NOTE_ON:
                        cueIndex = packetBytes[1];
                        OnCueStateChange?.Invoke(cueIndex, true);

                        Log.Debug($"Cue {cueIndex} turned on");
                        break;
                    case MIDI_NOTE_OFF:
                        cueIndex = packetBytes[1];
                        OnCueStateChange?.Invoke(cueIndex, false);

                        Log.Debug($"Cue {cueIndex} turned off");
                        break;
                    default:
                        Log.Debug("MIDI received: " + string.Join(" ", packetBytes.ToList().Select(b => b.ToString("X2"))));
                        break;
                }
            });
        }

        public async Task SendBeat(double tempo)
        {
            var delay = TimeSpan.FromTicks((long)(TimeSpan.TicksPerMinute / 24.0 / tempo) - 500);

            // TODO: Spin up a dedicated thread rather than relying on tasks
            for (int i = 0; i < 24; i++)
            {
                SendBeatTick();
                await Task.Delay(delay);
            }
        }

        public void SendBeatTick()
        {
            SendPacket(_tickPacket);
        }

        public async Task SendCue(int index)
        {
            if (index > 127 || index < 0)
                throw new ArgumentOutOfRangeException(nameof(index), index, $"{nameof(index)} must be between 0 and 127 (value provided {index})");

            byte note = (byte)(index % 128);

            var onPacket = MakePacket(MIDI_NOTE_ON, note, 0x70);
            SendPacket(onPacket);

            await Task.Delay(TimeSpan.FromMilliseconds(20));

            var offPacket = MakePacket(MIDI_NOTE_OFF, note, 0x70);
            SendPacket(offPacket);
        }

        public async Task SendValue(int channel, byte value)
        {
            if (channel > 119 || channel < 0)
                throw new ArgumentOutOfRangeException(nameof(channel), channel, $"{nameof(channel)} must be between 0 and 119 (value provided {channel})");
            if (value > 127)
                throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(value)} must be between 0 and 127 (value provided {value})");

            byte note = (byte)(channel % 128);
            value = (byte)(value % 128);

            var packet = MakePacket(MIDI_CONTINUOUS_CONTROLLER, note, value);
            SendPacket(packet);
        }

        private async Task SendStart()
        {
            OnConnect?.Invoke(this, null);

            // TODO: Support proper start/stop state from player
            var startPacket = MakePacket(MIDI_BEAT_START);
            SendPacket(startPacket);
        }

        private void SendPacket(MidiPacket packet)
        {
            if (_port != null)
                _port.Send(_endpoint, new[] { packet });
            else if (_endpoint != null)
                _endpoint.Received(new[] { packet });
        }

        private static MidiPacket MakePacket(byte status, params byte[] extraData)
        {
            var data = new byte[] { status }.Concat(extraData).ToArray();

            var packet = new MidiPacket(0, data);

            return packet;
        }
    }
}
